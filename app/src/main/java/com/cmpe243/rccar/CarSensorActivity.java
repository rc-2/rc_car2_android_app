package com.cmpe243.rccar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gospelware.compassviewlib.CompassView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Random;

public class CarSensorActivity extends AppCompatActivity {

    private static final String TAG ="CarSensorActivity" ;
    ImageView centerSensor;
    ImageView leftSensor;
    ImageView rightSensor;
    ImageView bottomSensor;
    CompassView compassView;
    TextView motorData;
    double latitude = 0;
    double longitude = 0;
    int[] imageArray = {
            R.drawable.baseline_signal_wifi_4_bar_24,
            R.drawable.twotone_signal_wifi_3_bar_24,
            R.drawable.twotone_signal_wifi_2_bar_24,
            R.drawable.twotone_signal_wifi_1_bar_24,
            R.drawable.baseline_signal_wifi_0_bar_24
    };
    boolean isDriving;
    boolean showControls;
    Button mBtnDriverControl;
    int returnIndex(int sensorValue){
        int index = 4;
        if (sensorValue<20){
            index = 0;
        }else if(sensorValue < 60){
            index = 1;
        }else if(sensorValue < 80){
            index = 2;
        }else if(sensorValue < 120){
            index = 3;
        }else{
            index = 4;
        }
        return index;


    }
    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }
    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event) {

        if(event.getmEventType() == MessageEvent.SENSOR_UPDATE){
            SensorData sensorData = (SensorData) event.getSensorData();
            int leftPosition = sensorData.leftSensor;
            int rightPosition = sensorData.rightSensor ;
            int centerPosition = sensorData.centerSensor ;
            int bottomPosition = sensorData.bottomSensor ;
            String debugData ="l:" + leftPosition + " c:" + centerPosition + " r:" + rightPosition + " c:" + bottomPosition;
            Log.d(TAG, debugData);
            centerSensor.setImageResource(imageArray[returnIndex(centerPosition)]);
            leftSensor.setImageResource(imageArray[returnIndex(leftPosition)]);
            rightSensor.setImageResource(imageArray[returnIndex(rightPosition)]);
            bottomSensor.setImageResource(imageArray[returnIndex(bottomPosition)]);

        }else if(event.getmEventType() == MessageEvent.COMPASS_UPDATE){
            CompassData compassData = event.getmCompassData();
            compassView.setRotation(compassData.mHeading);
        }else if(event.getmEventType() == MessageEvent.MOTOR_VALUES){
            MotorValues motorValues = event.getmMotorValues();
            if(motorValues != null){
                motorData.setText("" + motorValues.getmMotorSpeed() + "Miles/Hr");
            }
        }else if(event.getmEventType() == MessageEvent.START_DRIVE){
            mBtnDriverControl.setText("Stop Driving");
            isDriving = true;
        }else if(event.getmEventType() == MessageEvent.STOP_DRIVE){
            mBtnDriverControl.setText("Start Driving");
            isDriving = false;
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.car_sensor_view);

        centerSensor = findViewById(R.id.center_sensor);
        leftSensor = findViewById(R.id.center_left);
        rightSensor = findViewById(R.id.center_right);
        bottomSensor = findViewById(R.id.bottom);
        motorData = findViewById(R.id.speedData);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            latitude = extras.getDouble("LATITUDE");
            longitude = extras.getDouble("LONGITUDE");
            showControls = true;
        }else{
            showControls = false;
        }

        compassView = (CompassView) findViewById(R.id.compass);
        compassView.setRotation(0);

        Button sensorLog = findViewById(R.id.sensorLogBtn);
        sensorLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SensorLog.class);
                startActivity(intent);
            }
        });
        Button viewLocation = findViewById(R.id.locationUpdate);
        viewLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LocationActivity.class);
                intent.putExtra("LATITUDE", latitude  );
                intent.putExtra("LONGITUDE", longitude);
                startActivity(intent);
            }
        });

        mBtnDriverControl = findViewById(R.id.stopDriving);
        isDriving = true;
        mBtnDriverControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isDriving){
                    EventBus.getDefault().post(new MessageEvent(MessageEvent.STOP_DRIVE, null));
                }else{
                    EventBus.getDefault().post(new MessageEvent(MessageEvent.START_DRIVE, null));
                }
            }
        });
        if(!showControls){
            mBtnDriverControl.setVisibility(View.INVISIBLE);
        }
    }
}