package com.cmpe243.rccar;

public class SensorData implements IMessageData{
        public int leftSensor;
        public int rightSensor;
        public int centerSensor;
        public int bottomSensor;

        public SensorData(int left, int right, int center, int bottom){
            leftSensor = left;
            rightSensor = right;
            centerSensor = center;
            bottomSensor = bottom;
        }
    };