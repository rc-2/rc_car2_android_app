package com.cmpe243.rccar;

public class LocationData implements IMessageData {

    public double getmLattitude() {
        return mLattitude;
    }

    public double getmLongitude() {
        return mLongitude;
    }


    public LocationData(double lattitude, double longitude){
            mLattitude = lattitude;
            mLongitude = longitude;
    }

    private double mLattitude;
    private double mLongitude;
}
