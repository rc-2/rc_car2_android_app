package com.cmpe243.rccar;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class SensorLog extends AppCompatActivity {

    TextView mSensorView;
    MessageEvent messageEvent;
    public SensorLog() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }
    static Context mContext;
    static Handler mHandler;
    String sensorLog  = "";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_sensor_log);
        mHandler = new Handler();
        mSensorView  = (TextView) findViewById(R.id.sensorData);
        mSensorView.setMovementMethod(new ScrollingMovementMethod());

        final Runnable r = new Runnable() {
            public void run() {

                SensorData sensorData= MessageEvent.getSensorData();
                CompassData compassData = MessageEvent.getmCompassData();
                LocationData locationData = MessageEvent.getLocationData();
                MotorValues motorValues = MessageEvent.getmMotorValues();
                sensorLog  = "";
                if(sensorData != null){
                    sensorLog += "Sonar Left:" + sensorData.leftSensor + "\n";
                    sensorLog += "Sonar Right:" + sensorData.rightSensor + "\n";
                    sensorLog += "Sonar Center:" + sensorData.centerSensor + "\n";
                    sensorLog += "Sonar Behind:" + sensorData.bottomSensor + "\n\n\n\n";
                }
                if(compassData != null){
                    sensorLog += "Bearing   :" + compassData. mBearing+ "\n";
                    sensorLog += "Heading   :" + compassData.mHeading + "\n";
                    sensorLog += "Distance  :" + compassData.mDistanceRemaining + "\n";
                }
                if(locationData != null){
                    sensorLog += "Lat   :" + locationData.getmLattitude()+ "\n";
                    sensorLog += "Long   :" + locationData.getmLongitude() + "\n";
                }
                if(motorValues != null){
                    sensorLog += "Speed   :" + motorValues.getmMotorSpeed()+ "\n";
                    sensorLog += "Steering   :" + motorValues.getmSteering()+ "\n";
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(sensorLog.equals("")){
                            mSensorView.setText("No Data To Display");
                        }else{
                            mSensorView.setText(sensorLog);
                        }
                    }
                });


                mHandler.postDelayed(this, 1000);
            }
        };
        mHandler.postDelayed(r,400);

    }

}