package com.cmpe243.rccar;

public class MessageEvent {

    public static int CONNECTION_REQUEST = 0x21;
    public static int CONNECTED_EVENT = 0x22;
    public static int LOCATION_UPDATE = 0x23;
    public static int SENSOR_UPDATE = 0x24;
    public static int START_DRIVE = 0x25;
    public static int COMPASS_UPDATE = 0x26;
    public static int MOTOR_VALUES = 0x27;
    public static int DESTINATION_UPDATE = 0x28;
    public static int CLEAR_BUFFER = 0x29;
    public static int STOP_DRIVE = 0x30;
    public static int CAR_STATUS_CONNECTED = 0x31;
    public static int CAR_STATUS_DISCONNECTED = 0x32;

    public static SensorData getSensorData() {
        return mSensorData;
    }

    public static LocationData getLocationData() {
        return mLocationData;
    }

    public static LocationData getmCurrentLocation() {
        return mCurrentLocation;
    }

    public static LocationData getmDestination() {
        return mDestination;
    }

    public static CompassData getmCompassData() {
        return mCompassData;
    }

    public static MotorValues getmMotorValues() {
        return mMotorValues;
    }

    public MessageEvent(int eventType, IMessageData data) {

        if (eventType == SENSOR_UPDATE) {
            mSensorData = (SensorData) data;
        } else if (eventType == LOCATION_UPDATE) {
            mLocationData = (LocationData) data;
        } else if (eventType == CONNECTED_EVENT) {
            mCurrentLocation = (LocationData) data;
        } else if (eventType == COMPASS_UPDATE) {
            mCompassData = (CompassData) data;
        } else if (eventType == MOTOR_VALUES) {
            mMotorValues = (MotorValues) data;
        }else if(eventType == DESTINATION_UPDATE){
            mDestination = (LocationData) data;
        }

        mEventType = eventType;

    }

    public int getmEventType() {
        return mEventType;
    }

    static private SensorData mSensorData;
    static private LocationData mCurrentLocation;
    static private LocationData mLocationData;
    static private CompassData mCompassData;
    static private MotorValues mMotorValues;
    static private LocationData mDestination;

    static private int mEventType;
}