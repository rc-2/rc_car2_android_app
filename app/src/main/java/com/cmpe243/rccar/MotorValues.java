package com.cmpe243.rccar;

public class MotorValues  implements  IMessageData{
    public int getmMotorSpeed() {
        return mMotorSpeed;
    }

    public float getmSteering() {
        return mSteering;
    }

    private int mMotorSpeed;
    private float mSteering;
    public MotorValues(int motorSpeed, float steering){
        mMotorSpeed = motorSpeed;
        mSteering = steering;
    }
}
