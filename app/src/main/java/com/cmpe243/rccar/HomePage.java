package com.cmpe243.rccar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.media.Image;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


public class HomePage extends AppCompatActivity {
    boolean isConnected = false;
    private String TAG = "HomePage";
    ImageButton mConnectionBtn;
    TextView mConnectionStaus;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_page);


        mConnectionBtn = findViewById(R.id.connection);
        mConnectionStaus = findViewById(R.id.connectionStatus);
        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = service
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }

        Button connectRequest = findViewById(R.id.connectBtn);
        startService(new Intent(this, MqttClientService.class));
        connectRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new MessageEvent(MessageEvent.CONNECTION_REQUEST, null));
            }
        });



        Button sensorActivity = findViewById(R.id.viewCar);
        sensorActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CarSensorActivity.class);
                startActivity(intent);
            }
        });

    }
    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }
    @Subscribe
    public void onEvent(MessageEvent event) {
        if (event.getmEventType() == MessageEvent.CONNECTED_EVENT) {
             EventBus.getDefault().post(new MessageEvent(MessageEvent.CLEAR_BUFFER, null));
             isConnected = true;
             LocationData locationData= (LocationData) event.getmCurrentLocation();
             Log.d(TAG,"Lat:" + locationData.getmLattitude() + ", Lon:" + locationData.getmLongitude());
             Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
             intent.putExtra("LATITUDE", locationData.getmLattitude()  );
             intent.putExtra("LONGITUDE", locationData.getmLongitude());
             startActivity(intent);
        }else if(event.getmEventType() == MessageEvent.CAR_STATUS_CONNECTED){

            mConnectionBtn.setBackgroundColor(Color.parseColor("#69A550"));
            mConnectionBtn.setImageDrawable(getResources().getDrawable(R.drawable.baseline_done_white_24));
            mConnectionStaus.setText("Connected");
        }else if(event.getmEventType() == MessageEvent.CAR_STATUS_DISCONNECTED){
            mConnectionBtn.setBackgroundColor(Color.parseColor("#ec131e"));
            mConnectionBtn.setImageDrawable(getResources().getDrawable(R.drawable.baseline_clear_24));
            mConnectionStaus.setText("Disconnected");

        }
    }
}