package com.cmpe243.rccar;

public class CompassData implements IMessageData {
    public float mBearing;
    public float mHeading;
    public float mDistanceRemaining;

    public CompassData(float bearing, float heading, float distance){
        mBearing = bearing;
        mHeading = heading;
        mDistanceRemaining = distance;
    }

}
