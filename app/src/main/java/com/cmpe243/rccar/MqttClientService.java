package com.cmpe243.rccar;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class MqttClientService extends Service implements MqttCallbackExtended {

    private static final String STATUS_CHANNEL = "rc_car/status";
    MqttAndroidClient client;
    final String SERVER_URI = "tcp://34.70.69.177";
    final String COMMAND_CHANNEL = "rc_car/command";
    final String DATA_CHANNEL = "rc_car/can_data";
    final String TAG = "MQTT_CLIENT_SERVICE";
    boolean isAlive = false;
    Runnable runnableCode;
    Handler handler;
    boolean isReconnection = true;
    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Log.d(TAG, "Starting Service");
        client = new MqttAndroidClient(getApplicationContext(), SERVER_URI, MqttClient.generateClientId());
        client.setCallback(this);
        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setCleanSession(false);
        try {

            client.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
                    disconnectedBufferOptions.setBufferEnabled(true);
                    disconnectedBufferOptions.setBufferSize(100);
                    disconnectedBufferOptions.setPersistBuffer(false);
                    disconnectedBufferOptions.setDeleteOldestMessages(false);
                    client.setBufferOpts(disconnectedBufferOptions);
                    subscribeToTopic();
                    //Log.d(TAG, "Started Service");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    //Log.d(TAG, "Failed to Connect to Cloud Service");
                }
            });


        } catch (MqttException ex) {
            ex.printStackTrace();
        }

        handler = new Handler();
        runnableCode = new Runnable() {
            @Override
            public void run() {
                if(!isAlive){
                    EventBus.getDefault().post(new MessageEvent(MessageEvent.CAR_STATUS_DISCONNECTED, null));
                    isReconnection = true;
                }else{
                    if(isReconnection){
                        EventBus.getDefault().post(new MessageEvent(MessageEvent.CAR_STATUS_CONNECTED, null));
                        isReconnection = false;
                    }
                }
                isAlive = false;
                handler.postDelayed(runnableCode, 2000);
            }
        };
        EventBus.getDefault().register(this);
        return START_STICKY;
    }

    public void subscribeToTopic() {
        try {
            client.subscribe(COMMAND_CHANNEL, 0, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    //Log.d(TAG, "Subscribed!");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    //Log.d(TAG, "Subscribed!" + COMMAND_CHANNEL);
                }
            });
            client.subscribe(DATA_CHANNEL, 0, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    //Log.d(TAG, "Subscribed!" + DATA_CHANNEL);
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    //Log.d(TAG, "Failed to subscribe");
                }
            });
            client.subscribe(STATUS_CHANNEL, 0, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    //Log.d(TAG, "Subscribed!" + DATA_CHANNEL);
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    //Log.d(TAG, "Failed to subscribe");
                }
            });
        } catch (MqttException ex) {
            //Log.d(TAG, "Exception whilst subscribing");
            ex.printStackTrace();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void connectionLost(Throwable cause) {
        //Log.d(TAG, "Failed to Connec");
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        String messageStr = new String(message.getPayload());
        if (topic.equals("rc_car/status")) {
            Log.d(TAG, messageStr);
            isAlive = true;
            handler.post(runnableCode);
        } else {
            messageStr = messageStr.replace("\n", "").replace("\r", "");
            try{
                if(messageStr.startsWith("CURRENT_LOCATION")){
                    String data[] = messageStr.split(",");
                    //Log.d(TAG, messageStr);
                    float latitude = Float.parseFloat(data[1]);
                    float longitude = Float.parseFloat(data[2]);
                    LocationData locationData = new LocationData(latitude, longitude);
                    EventBus.getDefault().post(new MessageEvent(MessageEvent.CONNECTED_EVENT, locationData));
                }
                else if (messageStr.startsWith("LOCATION_UPDATE")) {
                    String data[] = messageStr.split(",");
                    //Log.d(TAG, ""+ data.length);
                    float latitude = Float.parseFloat(data[1]);
                    float longitude = Float.parseFloat(data[2]);
                    LocationData locationData = new LocationData(latitude, longitude);
                    EventBus.getDefault().post(new MessageEvent(
                            MessageEvent.LOCATION_UPDATE,
                            locationData));
                } else if (messageStr.startsWith("SENSOR_UPDATE")) {
                    String data[] = messageStr.split(",");
                    //Log.d(TAG, "" +  data.length);
                    int leftData =   Integer.parseInt(data[1]);
                    int centerData = Integer.parseInt(data[2]);
                    int rightData =  Integer.parseInt(data[3]);
                    int bottomData = Integer.parseInt(data[4]);
                    EventBus.getDefault().post(new MessageEvent(
                            MessageEvent.SENSOR_UPDATE,
                            new SensorData(leftData, rightData, centerData, bottomData)));
                }
                else if (messageStr.startsWith("COMPASS_HEADING")) {
                    String data[] = messageStr.split(",", -2);
                    float bearing = Float.parseFloat(data[1].trim());
                    float heading = Float.parseFloat(data[2].trim());
                    float distance = Float.parseFloat(data[3].trim());
                    EventBus.getDefault().post(new MessageEvent(
                            MessageEvent.COMPASS_UPDATE,
                            new CompassData(bearing, heading, distance)));
                }
                else if (messageStr.startsWith("MOTOR_VALUES")) {
                    String data[] = messageStr.split(",");
                    //Log.d(TAG, ""+ data.length);
                    int speed = Integer.parseInt(data[1]);
                    float steering = Float.parseFloat(data[2]);
                    MotorValues motorValues = new MotorValues(speed, steering);
                    EventBus.getDefault().post(new MessageEvent(
                            MessageEvent.MOTOR_VALUES,
                            motorValues));
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }

    @Override
    public void connectComplete(boolean reconnect, String serverURI) {
        //Log.d(TAG, serverURI);
    }


    @Subscribe(threadMode = ThreadMode.MAIN, priority = 1)
//    @Subscribe(priority = 1)
    public void onEvent(MessageEvent event) {
        //Log.d(TAG, "Publishing the Event");
        if (event.getmEventType() == MessageEvent.CONNECTION_REQUEST) {
            publishMessage("CONNECT_REQUEST|");
        }else if (event.getmEventType() ==  MessageEvent.START_DRIVE){
            publishMessage("START_DRIVE|");
        }else if(event.getmEventType() == MessageEvent.DESTINATION_UPDATE){
            LocationData destination = event.getmDestination();
            NumberFormat formatter = new DecimalFormat("0.0000");
            publishMessage("DESTINATION_MSG,"+ formatter.format(destination.getmLattitude()) + "," + formatter.format(destination.getmLongitude()) + "|");
        }else if(event.getmEventType() == MessageEvent.CLEAR_BUFFER){
            publishMessage("CLEAR_BUFFER|");
        }else if(event.getmEventType() == MessageEvent.STOP_DRIVE){
            publishMessage("STOP_DRIVE|");
        }
    }

    public void publishMessage(String payloadData) {
        //Log.d(TAG, "Sending Connection Request Event");
        try {
            MqttMessage message = new MqttMessage();
            message.setPayload(payloadData.getBytes());
            client.publish(COMMAND_CHANNEL, message);
        } catch (MqttException e) {
            System.err.println("Error Publishing: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
