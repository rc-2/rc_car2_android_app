package com.cmpe243.rccar;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements GoogleMap.OnMapClickListener, OnMapReadyCallback, GoogleMap.OnPolylineClickListener, GoogleMap.OnPolygonClickListener {

    private static final String TAG = "MapsActivty";
    private GoogleMap mMap;
    double latitude = 0;
    double longitude = 0;
    ArrayList<LatLng> pathNodes;
    ArrayList<Polyline> mPolyLines;
    private LocationManager locationManager;
    private String provider;
    Context mContext;

    float mBearing;

    final int MY_PERMISSIONS_REQUEST_LOCATION = 0x2bbb;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle(R.string.title_location_permission)
                        .setMessage(R.string.text_location_permission)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MapsActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {


                        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        // Define the criteria how to select the locatioin provider -> use
                        // default
                        Criteria criteria = new Criteria();
                        provider = locationManager.getBestProvider(criteria, false);
                        Location location = locationManager.getLastKnownLocation(provider);

                        // Initialize the location fields
                        if (location != null) {
                            System.out.println("Provider " + provider + " has been selected.");
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        } else {
                            Toast.makeText(this, "Failed to Get Location", Toast.LENGTH_SHORT);
                        }

                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.

                }
                return;
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        pathNodes = new ArrayList<>();
        mPolyLines = new ArrayList<>();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            latitude = extras.getDouble("LATITUDE");
            longitude = extras.getDouble("LONGITUDE");
            pathNodes.add(new LatLng(latitude, longitude));
            if (checkLocationPermission()) {
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location != null) {
                    mBearing = location.getBearing();
                }
            }
        } else {
            // Get the location manager
            if (checkLocationPermission()) {
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                // Define the criteria how to select the locatioin provider -> use
                // default
                Criteria criteria = new Criteria();
                provider = locationManager.getBestProvider(criteria, false);
                Location location = locationManager.getLastKnownLocation(provider);

                // Initialize the location fields
                if (location != null) {
                    System.out.println("Provider " + provider + " has been selected.");
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                } else {
                    Toast.makeText(this, "Failed to Get Location", Toast.LENGTH_SHORT);
                }
            }

        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(pathNodes.size()>2){
                    Log.d(TAG, "Starting to Drive");
                    EventBus.getDefault().post(new MessageEvent(MessageEvent.START_DRIVE, null));

                }else{
                    Toast.makeText(MapsActivity.this, "Add Destination", Toast.LENGTH_SHORT).show();
                }
            }
        });

        FloatingActionButton clearRoutes = findViewById(R.id.clear);
        clearRoutes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Starting to Drive");
                pathNodes.clear();
                EventBus.getDefault().post(new MessageEvent(MessageEvent.CLEAR_BUFFER, null));
                for(Polyline polyLine: mPolyLines)
                    polyLine.remove();
                mPolyLines.clear();
                pathNodes.add(new LatLng(latitude, longitude));
            }
        });
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        PolylineOptions options = new PolylineOptions().clickable(true);
        for (LatLng latLng : pathNodes) {
            options.add(latLng);
        }


        mPolyLines.add( googleMap.addPolyline(options));
        // Position the map's camera near Alice Springs in the center of Australia,
        // and set the zoom factor so most of Australia shows on the screen.
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 4));
        CameraPosition currentCamera = mMap.getCameraPosition();
        mMap.setOnMapClickListener(this);
        // Add a marker in Sydney and move the camera
        LatLng currentLocation = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(currentLocation).title("RC_CAR"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));
//        LatLngBounds latLongBounds = new LatLngBounds(
//                new LatLng(37.33313064888153, -121.88117955586688), // SW bounds
//                new LatLng(37.332606006322784, -121.8812171067907)  // NE bounds
//        );
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude))      // Sets the center of the map to Mountain View
                .zoom(22)                   // Sets the zoom
                .bearing(45)                // Sets the orientation of the camera to east
                .tilt(45)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        // Set listeners for click events.
        googleMap.setOnPolylineClickListener(this);
        googleMap.setOnPolygonClickListener(this);
    }

    @Override
    public void onPolylineClick(Polyline polyline) {

    }

    @Override
    public void onPolygonClick(Polygon polygon) {

    }

    @Override
    public void onMapClick(LatLng latLng) {
        pathNodes.add(latLng);
        PolylineOptions options = new PolylineOptions().clickable(true);
        for (LatLng co : pathNodes) {
            options.add(co);
        }
        mPolyLines.add( mMap.addPolyline(options));
        Log.d(TAG, "Destination_" + latLng.latitude + "," + latLng.longitude);
        EventBus.getDefault().post(new MessageEvent(MessageEvent.DESTINATION_UPDATE,
                new LocationData(latLng.latitude, latLng.longitude)));
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Subscribe
    public void onEvent(MessageEvent event) {
        if (event.getmEventType() == MessageEvent.START_DRIVE) {
            Log.d(TAG, pathNodes.toString());
            Intent intent = new Intent(getApplicationContext(), CarSensorActivity.class);
            intent.putExtra("LATITUDE", latitude);
            intent.putExtra("LONGITUDE", longitude);
            intent.putExtra("showControls", true);
            startActivity(intent);

        }
    }

}